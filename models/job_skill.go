package models

import (
	"database/sql"
)

type JobSkill struct {
	ID      int `json:"id" binding:"required"`
	SkillID int `json:"skill_id" binding:"required"`
	JobID   int `json:"job_id" binding:"required"`
}

func (cs *JobSkill) DeleteJobSkill(db *sql.DB) error {
	_, err := db.Exec("DELETE FROM job_skills WHERE job_id=$1 AND skill_id=$2", cs.JobID, cs.SkillID)
	if err != nil {
		return err
	}

	return nil
}

func (cs *JobSkill) CreateJobSkill(db *sql.DB) error {
	err := db.QueryRow(
		"INSERT INTO job_skills(skill_id, job_id) VALUES($1, $2) RETURNING id",
		cs.SkillID, cs.JobID).Scan(&cs.ID)

	if err != nil {
		return err
	}

	return nil
}

func (s *Skill) GetJobSkill(db *sql.DB, JobId string, skillId string) error {
	return db.QueryRow("SELECT skills.* FROM skills JOIN job_skills ON skills.id = job_skills.skill_id "+
		"WHERE job_skills.job_id=$1 AND job_skills.skill_id=$2", JobId, skillId).Scan(&s.ID, &s.Name)
}

func GetJobSkills(db *sql.DB, JobId string) ([]Skill, error) {
	rows, err := db.Query("SELECT skills.* FROM skills JOIN job_skills ON skills.id = job_skills.skill_id "+
		"WHERE job_skills.job_id=$1", JobId)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	skills := make([]Skill, 0)
	for rows.Next() {
		var s Skill
		if err := rows.Scan(&s.ID, &s.Name); err != nil {
			return nil, err
		}
		skills = append(skills, s)
	}

	return skills, nil
}
